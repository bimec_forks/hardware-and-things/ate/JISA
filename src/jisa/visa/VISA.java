package jisa.visa;

import com.sun.jna.Platform;
import jisa.Util;
import jisa.addresses.Address;
import jisa.addresses.TCPIPAddress;
import jisa.visa.connections.Connection;
import jisa.visa.drivers.*;

import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Static class for accessing VISA-like communication libraries
 */
public class VISA {

    private interface DriverInit {
        Driver create() throws VISAException;
    }

    private final static ArrayList<Driver>  loadedDrivers = new ArrayList<>();
    private final static Map<Class, Driver> lookup;

    private final static Map<String, DriverInit> DRIVERS = Util.buildMap(drivers -> {

        drivers.put("RS VISA",    RSVISADriver::new);
        drivers.put("AG VISA",    AGVISADriver::new);
        drivers.put("NI VISA",    NIVISADriver::new);
        drivers.put("Linux GPIB", LinuxGPIBDriver::new);
        drivers.put("NI GPIB",    NIGPIBDriver::new);
        drivers.put("Serial",     SerialDriver::new);
        drivers.put("TCP-IP",     TCPIPDriver::new);

        drivers.put("Experimental USB", () -> {

            boolean isLinux   = Platform.isLinux();
            boolean noVISA    = loadedDrivers.stream().noneMatch(d -> d instanceof VISADriver);
            boolean isEnabled = Paths.get(System.getProperty("user.home"), "jisa-usb.enable").toFile().exists();

            if (isLinux || noVISA || isEnabled) {
                return new USBDriver();
            } else {
                throw new VISAException("Not Enabled");
            }

        });

    });

    static {

        Locale.setDefault(Locale.US);
        System.out.println("Attempting to load drivers:");

        int maxLength = DRIVERS.keySet().stream().mapToInt(String::length).max().orElse(0);

        DRIVERS.forEach((name, inst) -> {

            System.out.printf("- Loading %s Driver...", name);

            for (int i = name.length(); i < maxLength; i++) {
                System.out.print(" ");
            }

            try {
                loadedDrivers.add(inst.create());
                System.out.println(" [Success].");
            } catch (VISAException exception) {
                System.out.printf(" [Failed] (%s).%n", exception.getMessage());
            }

        });

        lookup = loadedDrivers.stream().collect(Collectors.toMap(Driver::getClass, d -> d));

        if (loadedDrivers.isEmpty()) {
            System.out.println("No drivers loaded.");
        } else {
            System.out.printf("Successfully loaded %d drivers.%n", loadedDrivers.size());
        }

    }

    public static void init() {
    }

    public static void resetDrivers() {

        for (Driver driver : loadedDrivers) {

            try {
                driver.reset();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public static <T extends Driver> T getDriver(Class<T> driverClass) {
        return (T) lookup.getOrDefault(driverClass, null);
    }

    /**
     * Returns an array of all instrument addressed detected by VISA
     *
     * @return List of instrument addresses
     *
     * @throws VISAException Upon error with VISA interface
     */
    public static List<Address> listInstruments() throws VISAException {

        List<Address> addresses = new LinkedList<>();

        return loadedDrivers.stream()
                            .flatMap(d -> {
                                try { return d.search().stream(); } catch (Exception e) { return Stream.empty(); }
                            })
                            .map(a -> a.getJISAString().trim())
                            .distinct().map(Address::parse)
                            .collect(Collectors.toUnmodifiableList());

    }

    public static Connection openInstrument(Address address) throws VISAException {
        return openInstrument(address, null);
    }

    /**
     * Open the instrument with the given VISA resource address
     *
     * @param address Resource address
     *
     * @return Instrument handle
     *
     * @throws VISAException Upon error with VISA interface
     */
    public static Connection openInstrument(Address address, Class<? extends Driver> preferredDriver) throws VISAException {

        Connection        connection = null;
        ArrayList<String> errors     = new ArrayList<>();

        if (preferredDriver != null && lookup.containsKey(preferredDriver)) {

            try {
                connection = lookup.get(preferredDriver).open(address);
                return connection;
            } catch (Exception ignored) {}

        }

        // Workaround to use internal TCP-IP implementation since there seems to be issues with TCP-IP Sockets and NI-VISA
        if (address instanceof TCPIPAddress) {

            try {
                connection = lookup.get(TCPIPDriver.class).open(address);
                return connection;
            } catch (Exception ignored) {}

        }

        boolean tried = false;

        // Try each driver in order
        for (Driver d : loadedDrivers) {

            if (d.worksWith(address)) {

                tried = true;

                try {
                    connection = d.open(address);
                    break; // If it worked, then let's use it!
                } catch (VISAException e) {
                    errors.add(String.format("* %s: %s", d.getClass().getSimpleName(), e.getMessage()));
                }

            }

        }

        if (!tried) {
            throw new VISAException("No drivers available that support connecting to %s", address.toString());
        }

        // If no drivers worked
        if (connection == null) {
            throw new VISAException("Could not open %s using any driver%n%s", address.toString(), String.join("\n", errors));
        }

        return connection;

    }

}
